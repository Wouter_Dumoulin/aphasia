package com.exevan.aphasia.config;

import android.os.Environment;

/**
 * Created by Exevan on 20/12/2016.
 */

public abstract class Defaults {

    public static final String APP_ROOT = Environment.getExternalStorageDirectory() + "/Aphasia";

    public static final String TEMP_DIR = APP_ROOT + "/temp";

    public static final String DEFAULT_TEST_DIR = APP_ROOT + "/tests";
    public static final String DEFAULT_RESULT_DIR = APP_ROOT + "/results";

    public static final String TEST_TYPES = "picture,sentence,sound";
}
