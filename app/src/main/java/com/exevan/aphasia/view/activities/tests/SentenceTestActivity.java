package com.exevan.aphasia.view.activities.tests;

import android.content.res.AssetFileDescriptor;
import android.widget.ImageButton;
import android.widget.TextView;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.exevan.aphasia.R;
import com.exevan.aphasia.domain.steps.MultipleChoiceStepFactory;
import com.exevan.aphasia.io.IOUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SentenceTestActivity extends MultipleResponseTestActivity {

    @Override
    protected void generateTestSteps() {
        int steps = getTest().getSteps();

        AssetFileDescriptor afd = getResFile().getAssetFileDescriptor("test/hints.yaml");

        InputStream is;
        try {
            is = afd.createInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        InputStreamReader ir = new InputStreamReader(is);
        YamlReader yamlReader = new YamlReader(ir);

        Map<String, String> yaml;
        try {
            yaml = yamlReader.read(Map.class);
        } catch (YamlException e) {;
            throw new RuntimeException(e);
        }

        if(yaml.size() < steps)
            throw new RuntimeException("could not generate test steps, nb of available hints is smaller than nb of steps");

        Map<String, String> mapping = new HashMap<>(yaml.size());

        List<String> keys = new ArrayList<>(yaml.keySet());
        for (int i = 1; i <= steps; i++) {
            String key = keys.get(i-1);
            String hint = key;
            String response = IMAGE_DIR + yaml.get(key) + ".png";
            mapping.put(hint, response);
        }

        setTestSteps(new MultipleChoiceStepFactory().createRandomizedSteps(steps, mapping));
    }

    //--------NEXT TEST STEP------------------------------------------------------------------------


    @Override
    protected void populateTest() {

        String hint = getCurrentTestStep().getHint();
        List<String> responses = getCurrentTestStep().getResponses();

        TextView stepText = (TextView) findViewById(R.id.steptext);
        stepText.setText(getResources().getString(R.string.testlayout_step) + " " + (getCurrentStep() + 1) + "/" + getTest().getSteps());

        ImageButton testImg0 = (ImageButton) findViewById(R.id.testimg0);
        testImg0.setTag(responses.get(0));
        testImg0.setImageBitmap(IOUtil.getImageFromZip(responses.get(0), getTest().getPath()));

        ImageButton testImg1 = (ImageButton) findViewById(R.id.testimg1);
        testImg1.setTag(responses.get(1));
        testImg1.setImageBitmap(IOUtil.getImageFromZip(responses.get(1), getTest().getPath()));

        ImageButton testImg2 = (ImageButton) findViewById(R.id.testimg2);
        testImg2.setTag(responses.get(2));
        testImg2.setImageBitmap(IOUtil.getImageFromZip(responses.get(2), getTest().getPath()));

        ImageButton testImg3 = (ImageButton) findViewById(R.id.testimg3);
        testImg3.setTag(responses.get(3));
        testImg3.setImageBitmap(IOUtil.getImageFromZip(responses.get(3), getTest().getPath()));

        TextView hintText = (TextView) findViewById(R.id.hinttext);
        hintText.setText(hint);
    }

    private static final String IMAGE_DIR = "test/images/";

}
