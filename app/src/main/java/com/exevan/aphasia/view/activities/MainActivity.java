package com.exevan.aphasia.view.activities;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.exevan.aphasia.R;
import com.exevan.aphasia.config.Defaults;
import com.exevan.aphasia.domain.TestHandleFactory;
import com.exevan.aphasia.domain.TestComparator;
import com.exevan.aphasia.domain.tests.TestHandle;
import com.exevan.aphasia.io.IOUtil;
import com.exevan.aphasia.view.TestListAdapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ITestActivityLauncher {

    private java.util.Properties prop;
    private List<TestHandle> tests;

    //--------LIFECYCLE METHODS---------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        init();
        populateTestList();
    }

    //--------INITIALIZATION METHODS----------------------------------------------------------------

    private void init() {

        Log.i("Aphasia", "------initializing app-------------------------------------------------");

        if (!initRoot()) {
            Log.e("Aphasia", "initializing app root folder has failed, exiting app");
            System.exit(0);
        } else {
            Log.i("Aphasia", "initializing app root folder successful");
        }

        if (!initProps()) {
            Log.e("Aphasia", "initializing properties has failed, exiting app");
            System.exit(0);
        } else {
            Log.i("Aphasia", "initializing properties successful");
        }

        if (!initDirs()) {
            Log.e("Aphasia", "initializing directories has failed, exiting app");
            System.exit(0);
        } else {
            Log.i("Aphasia", "initializing directories successful");
        }

        if (!initTests()) {
            Log.e("Aphasia", "initializing test catalog has failed, exiting app");
            System.exit(0);
        } else {
            Log.i("Aphasia", "initializing test catalog successful");
        }

        Log.i("Aphasia", "------initializing complete--------------------------------------------");

    }

    private boolean initRoot() {

        Log.i("Aphasia", "--------initializing app root------------------------------------------");

        //check if external storage media is mounted
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Log.e("Aphasia", "Could not start app: external storage is not mounted");
            return false;
        }

        File appRoot = new File(Defaults.APP_ROOT);
        String path = appRoot.getAbsolutePath();

        //create app root folder if it does not yet exist
        Log.i("Aphasia", "app root dir exists: " + appRoot.exists());
        if (!appRoot.exists()) {
            Log.i("Aphasia", "app root dir " + path + " does not exist, creating");
            if (appRoot.mkdirs()) {
                Log.i("Aphasia", "app root dir " + path + " created successfully");
                IOUtil.scanFile(getApplicationContext(), appRoot);
            } else {
                Log.e("Aphasia", "app root dir " + path + " creation failed");
                return false;
            }
        }

        //check app root write permission
        Log.i("Aphasia", "checking write permission");
        if (appRoot.canWrite()) {
            Log.i("Aphasia", "app has write permission in " + path);
        } else {
            Log.e("Aphasia", "app has no write permission in " + path);
            return false;
        }

        return true;
    }

    private boolean initProps() {

        Log.i("Aphasia", "--------initializing properties----------------------------------------");

        try {
            File propFile = new File(Defaults.APP_ROOT + "/config.properties");
            prop = new java.util.Properties();

            if(propFile.exists()) {
                Log.i("Aphasia", "config.properties found, loading properties");
                prop.load(new FileInputStream(propFile));
            } else {
                Log.i("Aphasia", "config.properties not found, creating file");
                prop = new java.util.Properties();
                prop.setProperty("testdir", Defaults.DEFAULT_TEST_DIR);
                prop.setProperty("resultdir", Defaults.DEFAULT_RESULT_DIR);
                prop.setProperty("testtypes", Defaults.TEST_TYPES);
                prop.store(new FileOutputStream(propFile), "configuration file for Aphasia app\n#to reset to default config, remove this file");
                IOUtil.scanFile(getApplicationContext(), propFile);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean initDirs() {

        Log.i("Aphasia", "--------initializing directories---------------------------------------");

        boolean success = true;

        success = success && initDir(prop.getProperty("testdir"));
        success = success && initDir(prop.getProperty("resultdir"));
        success = success && initDir(Defaults.APP_ROOT + "/temp");

        return success;
    }

    private boolean initDir(String path) {
        File dir = new File(path);
        Log.i("Aphasia", dir.getAbsolutePath() + "exists: " + dir.exists());
        if (!dir.exists()) {
            if (dir.mkdirs()) {
                Log.i("Aphasia", "test dir " + dir.getAbsolutePath() + " created successfully");
            } else {
                Log.e("Aphasia", "test dir " + dir.getAbsolutePath() + " creation failed");
                return false;
            }
            IOUtil.scanFile(getApplicationContext(), dir);
        }

        return true;
    }

    private boolean initTests() {

        Log.i("Aphasia", "--------initializing test catalog--------------------------------------");

        File testDir = new File(prop.getProperty("testdir"));

        Log.i("Aphasia", "loading tests in " + testDir.getAbsolutePath());

        if (!testDir.exists()) {
            Log.e("Aphasia", "could not load tests, " + testDir.getAbsolutePath() + " does not exist");
            return false;
        }

        if (!testDir.isDirectory()) {
            Log.e("Aphasia", "could not load tests, " + testDir.getAbsolutePath() + " is not a directory");
            return false;
        }

        if (!testDir.canRead()) {
            Log.e("Aphasia", "could not load tests, " + testDir.getAbsolutePath() + " is not readable");
            return false;
        }

        File[] testFiles = testDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.split("\\.")[1].equals("zip");
            }
        });

        Log.i("Aphasia", "found " + testFiles.length + " potential test files");

        List<TestHandle> tests = new ArrayList<>();
        for (File testFile : testFiles) {

            TestHandle test = loadTest(testFile);
            if (test != null)
                tests.add(test);
        }

        Collections.sort(tests, TestComparator.getTestNameComparator());
        Collections.sort(tests, TestComparator.getTestTypeComparator());
        Log.i("Aphasia", "loading tests complete");

        this.tests = tests;
        return true;
    }

    private TestHandle loadTest(File testFile) {
        try {
            ZipResourceFile zrf = new ZipResourceFile(testFile.getAbsolutePath());
            ZipResourceFile.ZipEntryRO[] zes = zrf.getAllEntries();
            ZipResourceFile.ZipEntryRO ze = null;
            for (int i = 0; i < zes.length; i++) {

                //test if any files in the zip are compressed
                if (!zes[i].isUncompressed()) {
                    Log.w("Aphasia", "could not load " + testFile.getName() + " because " + zes[i].mFileName + " is compressed instead of stored");
                    return null;
                }

                //test if meta.yaml is present
                if (zes[i].mFileName.equals("meta.yaml")) {
                    ze = zes[i];
                    break;
                }
            }

            if (ze == null) {
                Log.w("Aphasia", "no meta.yaml discovered in " + testFile.getName() + ", ignoring");
                return null;
            } else {

                TestHandleFactory fact = new TestHandleFactory();

                TestHandle test = fact.createTestHandleFromFile(testFile);

                if(test == null) {
                    return null;
                }

                Log.i("Aphasia", "successfully loaded " + test.getName());

                return test;
            }

        } catch (Exception e) {
            Log.e("Aphasia", Log.getStackTraceString(e));
            return null;
        }
    }

    private void populateTestList() {
        ListView lv = (ListView) findViewById(R.id.testlist);
        ArrayAdapter<TestHandle> adapter = new TestListAdapter(this, tests);
        lv.setAdapter(adapter);
    }

    //--------UI METHODS----------------------------------------------------------------------------


    @Override
    public void launchTestActivity(TestHandle test) {

        Class clazz = TestHandleFactory.getActivityClassForTest(test);
        if (clazz == null) {
            return;
        }

        Intent intent = new Intent(this, clazz);
        intent.putExtra("test", test);
        intent.putExtra("resultdir", prop.getProperty("resultdir"));

        Log.i("Aphasia", "launching test activity for " + test.getName());
        startActivity(intent);
    }

    //--------UTILITY METHODS-----------------------------------------------------------------------

}
