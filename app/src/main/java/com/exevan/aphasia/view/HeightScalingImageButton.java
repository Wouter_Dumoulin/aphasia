package com.exevan.aphasia.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageButton;

/**
 * TODO: document your custom view class.
 */
public class HeightScalingImageButton extends ImageButton {

    public HeightScalingImageButton(Context context) {
        super(context);
    }

    public HeightScalingImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeightScalingImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable d = getDrawable();
        if (d != null) {
            int h = MeasureSpec.getSize(heightMeasureSpec);
            int w = h * d.getIntrinsicWidth() / d.getIntrinsicHeight();
            setMeasuredDimension(w, h);
        }
        else super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
