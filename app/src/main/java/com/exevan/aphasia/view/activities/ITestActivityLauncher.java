package com.exevan.aphasia.view.activities;

import com.exevan.aphasia.domain.tests.TestHandle;

/**
 * Created by Exevan on 22/12/2016.
 */

public interface ITestActivityLauncher {

    void launchTestActivity(TestHandle test);

}
