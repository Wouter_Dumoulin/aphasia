package com.exevan.aphasia.view.activities.tests;

import android.media.MediaRecorder;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.exevan.aphasia.R;
import com.exevan.aphasia.domain.steps.TestStep;
import com.exevan.aphasia.io.IOUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PictureTestActivity extends OpenResponseTestActivity {

    private MediaRecorder mRecorder;


    @Override
    protected void generateTestSteps() {
        int steps = getTest().getSteps();
        Log.i("Aphasia", "steps: " + steps);

        List<ZipResourceFile.ZipEntryRO> entries = new ArrayList<>(Arrays.asList(getResFile().getAllEntries()));
        List<ZipResourceFile.ZipEntryRO> hints = new ArrayList<>();

        for(ZipResourceFile.ZipEntryRO entry : entries) {
            if(entry.mFileName.startsWith(IMAGE_DIR) && entry.mFileName.endsWith(".png")) {
                hints.add(entry);
            }
        }

        if(hints.size() < steps)
            throw new RuntimeException("could not generate test steps, nb of available images is smaller than nb of steps");

        Collections.shuffle(hints);

        for (int i = 1; i <= steps; i++) {
            TestStep step = new TestStep(i, hints.get(i-1).mFileName);
            Log.i("Aphasia", step.toString());
            addTestStep(step);
        }
    }

    @Override
    protected void reset() {
        super.reset();
        disableNextButton();
    }

    @Override
    protected void populateTest() {

        String hint = getCurrentTestStep().getHint();

        TextView stepText = (TextView) findViewById(R.id.steptext);
        stepText.setText(getResources().getString(R.string.testlayout_step) + " " + (getCurrentStep() + 1) + "/" + getTest().getSteps());

        ImageView hintImg = (ImageView) findViewById(R.id.hintimg);
        hintImg.setImageBitmap(IOUtil.getImageFromZip(hint, getTest().getPath()));

        Button recordBtn = (Button) findViewById(R.id.recordbtn);
        recordBtn.setTag(IOUtil.getFileNameFromPath(hint));

        disableNextButton();
    }

    public void startRecording(View view) {
        Button recordBtn = (Button) view;
        recordBtn.setText(R.string.picturetest_testlayout_recording);
        recordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording(v);
            }
        });
        disableNextButton();

        String hint = (String) view.getTag();
        recordResponse(hint);
    }

    public void recordResponse(String hint) {

        disableRecordButton();

        String path = getTempResultDir().getAbsolutePath() + "/response" + getCurrentStep() + ".mp3";
        Button nextBtn = (Button) findViewById(R.id.nextbtn);
        nextBtn.setTag(path);

        Log.i("Aphasia", "starting recording");
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile(path);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e("Aphasia", Log.getStackTraceString(e));
            mRecorder.release();
            return;
        }

        mRecorder.start();

        try {
            Thread.sleep(400);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        enableRecordButton();
    }

    public void stopRecording(View view) {
        Log.i("Aphasia", "stopping recording");
        mRecorder.stop();
        mRecorder.release();

        IOUtil.scanFile(getApplicationContext(), new File(getTempResultDir().getAbsolutePath() + "/response" + getCurrentStep() + ".mp3"));

        enableNextButton();

        Button recordBtn = (Button) view;
        recordBtn.setText(R.string.picturetest_testlayout_record);
        recordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRecording(v);
            }
        });
    }

    private void enableRecordButton() {
        Button recordBtn = (Button) findViewById(R.id.recordbtn);
        recordBtn.setEnabled(true);
    }

    private void disableRecordButton() {
        Button recordBtn = (Button) findViewById(R.id.recordbtn);
        recordBtn.setEnabled(false);
    }

    private void enableNextButton() {
        Button nextBtn = (Button) findViewById(R.id.nextbtn);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNextTestStep(v);
            }
        });
    }

    private void disableNextButton() {
        Button nextBtn = (Button) findViewById(R.id.nextbtn);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), R.string.picturetest_testlayout_pleaserecordresponsefirst, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private static final String IMAGE_DIR = "test/images/";

    private void scanFile(File file) {
        //let newly created folders be detected by android media store
        //(specifically so that when the device is connected to a pc, the pc can see the new folders immediately instead of after a device reboot)
        MediaScannerConnection.scanFile(this,
                new String[]{file.getAbsolutePath()},
                null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("Aphasia", "added " + path + " to android media store");
                    }
                });
    }
}
