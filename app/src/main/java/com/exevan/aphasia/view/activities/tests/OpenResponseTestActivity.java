package com.exevan.aphasia.view.activities.tests;

import android.os.Bundle;

import com.exevan.aphasia.domain.steps.TestStep;

import java.io.File;
import java.util.ArrayList;

public abstract class OpenResponseTestActivity extends AphasiaTestActivity {

    private ArrayList<TestStep> steps = new ArrayList<>();

    public TestStep getTestStep(int i) {
        return steps.get(i);
    }

    @Override
    public TestStep getCurrentTestStep() {
        return getTestStep(getCurrentStep());
    }

    public void addTestStep(TestStep step) {
        steps.add(step);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("steps", steps);
    }

    @Override
    protected void restoreActivity(Bundle savedInstanceState) {
        steps = savedInstanceState.getParcelableArrayList("steps");
        super.restoreActivity(savedInstanceState);
    }

    protected File getTempResultDir() {
        return getResultCollector().getTempResultDir();
    }
}
