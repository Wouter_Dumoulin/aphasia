package com.exevan.aphasia.view.activities.tests;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.exevan.aphasia.R;
import com.exevan.aphasia.domain.results.ResultCollector;
import com.exevan.aphasia.domain.steps.TestStep;
import com.exevan.aphasia.domain.tests.TestHandle;

import java.io.File;
import java.io.IOException;

public abstract class AphasiaTestActivity extends AppCompatActivity {

    private int state = -1;
    private int[] layouts = new int[3];

    private int currentStep = 0;

    private TestHandle test;
    private ZipResourceFile resFile;
    private ResultCollector resultCollector;

    protected SharedPreferences data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.setContentView(layouts[state]);
        this.state = state;
        switch (state) {
            case STATE_INTRO:
                populateIntro();
                break;
            case STATE_STEP:
                populateTest();
                break;
        }
    }

    public int getCurrentStep() {
        return currentStep;
    }

    public TestHandle getTest() {
        return test;
    }

    public ZipResourceFile getResFile() {
        return resFile;
    }

    public ResultCollector getResultCollector() {
        return resultCollector;
    }

    protected void setLayouts() {
        layouts[STATE_INTRO] = this.getResources().getIdentifier("activity_test_introlayout", "layout", this.getPackageName());
        layouts[STATE_STEP] = this.getResources().getIdentifier("activity_test_" + test.getType() + "test", "layout", this.getPackageName());
        layouts[STATE_OUTRO] = this.getResources().getIdentifier("activity_test_outrolayout", "layout", this.getPackageName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
            initActivity();
        else
            restoreActivity(savedInstanceState);

        setTitle(test.getName());
    }

    private void initActivity() {
        this.test = (TestHandle) getIntent().getSerializableExtra("test");
        this.resultCollector = new ResultCollector(getApplicationContext(), test, (String) getIntent().getSerializableExtra("resultdir"));

        try {
            this.resFile = new ZipResourceFile(test.getPath());
            this.generateTestSteps();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Log.i("Aphasia", "current step: " + getCurrentStep());

        setLayouts();
        setState(STATE_INTRO);
    }

    protected void restoreActivity(Bundle savedInstanceState) {

        this.currentStep = savedInstanceState.getInt("currentstep");
        this.test = (TestHandle) savedInstanceState.get("test");
        this.resultCollector = (ResultCollector) savedInstanceState.get("result");

        try {
            this.resFile = new ZipResourceFile(test.getPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        setLayouts();
        setState(savedInstanceState.getInt("state"));


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("state", state);
        outState.putInt("currentstep", getCurrentStep());
        outState.putSerializable("test", getTest());
        outState.putParcelable("result", getResultCollector());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        resultCollector.clearTempDir();
    }

    protected abstract void generateTestSteps();

    //--------START TEST----------------------------------------------------------------------------

    public void startTest(View view) {
        setState(STATE_STEP);
        reset();
        startNextTestStep();
    }

    public void restartTest(View view) {
        reset();
        setState(STATE_INTRO);
    }

    protected void reset() {
        currentStep = 0;
    }

    //--------NEXT TEST STEP------------------------------------------------------------------------

    public abstract TestStep getTestStep(int i);

    public abstract TestStep getCurrentTestStep();

    public void startNextTestStep() {
        startNextTestStep(null);
    }

    public void startNextTestStep(View view) {

        if (view != null) {
            String response = (String) view.getTag();
            getResultCollector().addResult(getCurrentStep() + 1, getTestStep(currentStep).getHint(), response);
            currentStep++;
        }

        Log.i("Aphasia", "current step: " + getCurrentStep());
        if (getCurrentStep() < getTest().getSteps()) {
            populateTest();

        } else {
            endTest();
        }
    }

    protected void populateIntro() {
        TextView stepText = (TextView) findViewById(R.id.introtext);
        stepText.setText(getResources().getString(this.getResources().getIdentifier(test.getType() + "test_introlayout_intro", "string", this.getPackageName())));
    }

    protected abstract void populateTest();

    //--------STOP TEST-----------------------------------------------------------------------------

    public void endTest() {

        Log.i("Aphasia", "results for " + getTest().getName());
        for (int i = 0; i < getTest().getSteps(); i++) {
            Log.i("Aphasia", getResultCollector().getHint(i) + " : " + getResultCollector().getResponse(i));
        }

        resultCollector.saveResults();

        setState(STATE_OUTRO);
    }

    public void launchMainActivity(View view) {
        finish();
    }

    public void showSendResult(View view) {
        if (state != STATE_OUTRO)
            throw new IllegalStateException();

        ((Button) view).setText(R.string.outrolayout_hide);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSendResult(v);
            }
        });

        View sendResultBox = findViewById(R.id.sendresultbox);
        sendResultBox.setVisibility(View.VISIBLE);
    }

    public void hideSendResult(View view) {
        if (state != STATE_OUTRO)
            throw new IllegalStateException();

        ((Button) view).setText(R.string.outrolayout_showsendresult);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSendResult(v);
            }
        });

        View sendResultBox = findViewById(R.id.sendresultbox);
        sendResultBox.setVisibility(View.GONE);
    }

    public void sendResult(View view) {
        if (state != STATE_OUTRO)
            throw new IllegalStateException();

        EditText emailValidate = (EditText)findViewById(R.id.emailedittext);
        String email = emailValidate.getText().toString().trim();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (! email.matches(emailPattern))
            Toast.makeText(getApplicationContext(), R.string.outrolayout_invalidemail, Toast.LENGTH_SHORT).show();
        else
            sendResult(email);
    }

    private void sendResult(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {email});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Aphasia result " + test.getName());
        emailIntent.putExtra(Intent.EXTRA_TEXT, "This mail has been sent from the Aphasia app.\nIt contains the result of " + test.getName() + ".");
        File file = new File(resultCollector.getResultFile());
        if (!file.exists() || !file.canRead()) {
            return;
        }
        Uri uri = Uri.fromFile(file);
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(emailIntent, "Pick an Email provider"));
    }

    public static final int STATE_INTRO = 0;
    public static final int STATE_STEP = 1;
    public static final int STATE_OUTRO = 2;

}
