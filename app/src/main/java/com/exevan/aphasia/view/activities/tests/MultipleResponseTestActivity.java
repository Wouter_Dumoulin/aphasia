package com.exevan.aphasia.view.activities.tests;

import android.os.Bundle;

import com.exevan.aphasia.domain.steps.MultipleChoiceStep;

import java.util.ArrayList;
import java.util.List;

public abstract class MultipleResponseTestActivity extends AphasiaTestActivity {

    private ArrayList<MultipleChoiceStep> steps;

    public MultipleChoiceStep getTestStep(int i) {
        return steps.get(i);
    }

    @Override
    public MultipleChoiceStep getCurrentTestStep() {
        return getTestStep(getCurrentStep());
    }

    public List<MultipleChoiceStep> getSteps() {
        return new ArrayList<>(steps);
    }

    public void setTestSteps(List<MultipleChoiceStep> steps) {
        this.steps = new ArrayList<>(steps);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("steps", steps);
    }

    @Override
    protected void restoreActivity(Bundle savedInstanceState) {
        steps = savedInstanceState.getParcelableArrayList("steps");
        super.restoreActivity(savedInstanceState);
    }

}
