package com.exevan.aphasia.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.exevan.aphasia.R;
import com.exevan.aphasia.domain.tests.TestHandle;
import com.exevan.aphasia.view.activities.ITestActivityLauncher;

import java.util.List;


public class TestListAdapter extends ArrayAdapter<TestHandle> {

    public TestListAdapter(Context context, List<TestHandle> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TestHandle test = getItem(position);

        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_test, parent, false);

        ((TextView) convertView.findViewById(R.id.nametext)).setText(test.getName());
        ((TextView) convertView.findViewById(R.id.typetext)).setText(test.getType());
        ((TextView) convertView.findViewById(R.id.desctext)).setText(test.getDesc());


        convertView.findViewById(R.id.itemcontainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ITestActivityLauncher) getContext()).launchTestActivity(test);
            }
        });


        return convertView;
    }
}
