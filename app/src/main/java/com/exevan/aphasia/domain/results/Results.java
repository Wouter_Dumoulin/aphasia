package com.exevan.aphasia.domain.results;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Exevan on 5/01/2017.
 */

public class Results implements Serializable{

    public String testName;
    public List<ResultEntry> entries;

    public Results() {
        this.entries = new ArrayList<>();
    }

    public Results (String testName) {
        this.testName = testName;
        this.entries = new ArrayList<>();
    }

    public String getTestName() {
        return testName;
    }

    public List<ResultEntry> getEntries() {
        return entries;
    }

    public String getHint(int i) {
        return entries.get(i).getHint();
    }

    public String getResponse(int i) {
        return entries.get(i).getResponse();
    }

    public void addEntry(int step, String hint, String response) {
        entries.add(new ResultEntry(step, hint, response));
    }

}
