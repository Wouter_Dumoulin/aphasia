package com.exevan.aphasia.domain.steps;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Exevan on 31/12/2016.
 */

public class MultipleChoiceStepFactory {

    public List<MultipleChoiceStep> createRandomizedSteps(int nbSteps, Map<String, String> mapping) {

        List<MultipleChoiceStep> steps = new ArrayList<>();

        List<String> keys = new ArrayList<>(mapping.keySet());
        Collections.shuffle(keys);
        for (int i = 1; i <= nbSteps; i++) {
            String hint = keys.get(i-1);
            List<String> responses = getResponsesForHint(hint, mapping);
            steps.add(new MultipleChoiceStep(i, hint, responses));
        }

        return steps;
    }

    public List<String> getResponsesForHint(String hint, Map<String, String> mapping) {
        Log.i("Aphasia", "--------building response list for " + hint + "---------------------------");
        String fixedResponse = mapping.get(hint);
        List<String> source = new ArrayList<>(mapping.values());
        source.remove(fixedResponse);
        List<String> randomResponses = buildRandomizedResponseListFrom(source, RANDOMIZED_RESPONSES_PER_HINT);
        Log.i("Aphasia", "adding " + fixedResponse + " to response list");
        randomResponses.add(fixedResponse);
        Collections.shuffle(randomResponses, new Random());
        return randomResponses;
    }

    private List<String> buildRandomizedResponseListFrom(List<String> source, int amount) {
        if (amount == 0) {
            return new ArrayList<>();
        } else {
            Random rand = new Random();
            String response = source.get(rand.nextInt(source.size()));
            source.remove(response);
            List<String> randomResponses = buildRandomizedResponseListFrom(source, amount - 1);
            Log.i("Aphasia", "adding " + response + " to response list");
            randomResponses.add(response);
            return randomResponses;
        }
    }

    private static final int RANDOMIZED_RESPONSES_PER_HINT = 3;
}
