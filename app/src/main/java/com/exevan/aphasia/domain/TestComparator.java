package com.exevan.aphasia.domain;

import com.exevan.aphasia.domain.tests.TestHandle;

import java.util.Comparator;

/**
 * Created by Exevan on 21/12/2016.
 */

public abstract class TestComparator implements Comparator<TestHandle> {

    public static TestComparator getTestNameComparator() {
        return new TestComparator() {
            @Override
            public int compare(TestHandle t1, TestHandle t2) {
                return t1.getName().compareTo(t2.getName());
            }
        };
    }

    public static TestComparator getTestTypeComparator() {
        return new TestComparator() {
            @Override
            public int compare(TestHandle t1, TestHandle t2) {
                return t1.getClass().getName().compareTo(t2.getClass().getName());
            }
        };
    }
}
