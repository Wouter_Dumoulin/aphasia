package com.exevan.aphasia.domain.tests;

import java.io.Serializable;

/**
 * Created by Exevan on 31/12/2016.
 */

public class TestHandle implements Serializable {

    private String name;
    private String desc;
    private int steps;
    private String type;
    private String path;

    public TestHandle(String name, String desc, int steps, String type, String path) {
        this.name = name;
        this.desc = desc;
        this.steps = steps;
        this.type = type;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public int getSteps() {
        return steps;
    }

    public String getType() {
        return type;
    }

    public String getPath() {
        return path;
    }

    @Override
    public String toString() {
        return "name: " + name
                + "\ndesc: " + desc
                + "\nsteps: " + steps
                + "\ntype: " + type
                + "\npath: " + path;
    }
}
