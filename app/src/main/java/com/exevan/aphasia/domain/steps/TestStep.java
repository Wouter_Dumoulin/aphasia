package com.exevan.aphasia.domain.steps;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by Exevan on 31/12/2016.
 */

public class TestStep implements Parcelable {

    private int nr;
    private String hint;

    public TestStep(int nr, String hint) {
        this.nr = nr;
        this.hint = hint;
    }

    protected TestStep(Parcel in) {
        this.nr = in.readInt();
        this.hint = in.readString();
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    @Override
    public String toString() {
        return "step nr: " + getNr() + ", hint: " + getHint();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(nr);
        dest.writeString(hint);
    }

    public static final Parcelable.Creator<TestStep> CREATOR = new Parcelable.Creator<TestStep>() {

        @Override
        public TestStep createFromParcel(Parcel in) {
            return new TestStep(in);
        }

        @Override
        public TestStep[] newArray(int size) {
            return new TestStep[size];
        }
    };
}
