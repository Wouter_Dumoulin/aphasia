package com.exevan.aphasia.domain;

import android.content.res.AssetFileDescriptor;
import android.util.Log;

import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.esotericsoftware.yamlbeans.YamlReader;
import com.exevan.aphasia.domain.tests.TestHandle;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * Created by Exevan on 19/12/2016.
 */

public class TestHandleFactory {

    public TestHandle createTestHandleFromFile(File testFile) throws IllegalArgumentException {

        Map<String, String> meta = loadMeta(testFile);

        if (! isValidMeta(meta)) {
            return null;
        }

        String type = meta.get(FIELD_TYPE);
        String name = meta.get(FIELD_NAME);
        int steps = Integer.parseInt(meta.get(FIELD_STEPS));
        String desc = meta.get(FIELD_DESC);
        String path = meta.get(FIELD_PATH);

        return new TestHandle(name, desc, steps, type, path);

    }

    private Map<String, String> loadMeta(File f) {

        try {
            ZipResourceFile zrf = new ZipResourceFile(f.getAbsolutePath());
            //get asset file descriptor for meta.yaml from zip file
            AssetFileDescriptor afd = zrf.getAssetFileDescriptor("meta.yaml");
            //get input stream from asset file descriptor
            InputStream i = afd.createInputStream();
            //wrap input stream in input stream reader to decode binary data to character data
            InputStreamReader ir = new InputStreamReader(i);
            //wrap input stream reader in yaml reader to parse yaml data
            YamlReader yr = new YamlReader(ir);
            //read yaml data and store as key-value pairs and return
            Map<String, String> meta =  yr.read(Map.class);
            meta.put("name", f.getName().split("\\.")[0]);
            meta.put("path", f.getAbsolutePath());
            return meta;
        } catch (IOException E) {
            Log.e("Aphasia", "could not create meta information");
            return null;
        }
    }

    private boolean isValidMeta(Map<String, String> meta) {

        String name = meta.get(FIELD_NAME);

        for (String field : REQUIRED_FIELDS) {
            if (!meta.containsKey(field)) {
                Log.w("Aphasia", "could not build test " + name + ", meta.yaml does not contain field \"" + field + "\"");
                return false;
            }

            if (meta.get(field) == null || meta.get(field).isEmpty()) {
                Log.w("Aphasia", "could not build test " + name + ", field \"" + field + "\" is null or empty");
                return false;
            }
        }

        return true;
    }

    private static String getClassNameForType(String type) {
        return TEST_PACKAGE + "." + type.substring(0, 1).toUpperCase() + type.substring(1) + "Test";
    }

    public static Class getActivityClassForTest(TestHandle test) {
        String type = test.getType();

        String className = ACTIVITY_PACKAGE + "." + type.substring(0, 1).toUpperCase() + type.substring(1) + "TestActivity";
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            Log.e("Aphasia", "could not create activity class for " + test.getName());
            return null;
        }
    }


    public static final String FIELD_TYPE = "type";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_DESC = "desc";
    public static final String FIELD_STEPS = "steps";
    public static final String FIELD_PATH = "path";
    public static final String[] REQUIRED_FIELDS = new String[]{FIELD_TYPE, FIELD_NAME, FIELD_DESC, FIELD_STEPS, FIELD_PATH};

    private static final String TEST_PACKAGE = "com.exevan.aphasia.domain.tests";
    private static final String ACTIVITY_PACKAGE = "com.exevan.aphasia.view.activities.tests";
}
