package com.exevan.aphasia.domain.steps;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Exevan on 31/12/2016.
 */

public class MultipleChoiceStep extends TestStep {

    private List<String> responses;

    public MultipleChoiceStep(int nr, String hint, List<String> responses) {
        super(nr, hint);

        if (responses.size() == NB_RESPONSES)
            this.responses = responses;
        else
            throw new IllegalArgumentException("cannot create MultipleChoiceStep, response list has size " + responses.size() + ", should be " + NB_RESPONSES);
    }

    protected MultipleChoiceStep(Parcel in) {
        super(in);
        for (int i = 0; i < NB_RESPONSES; i++) {
            responses.add(in.readString());
        }
    }

    public String getCorrectResponse() {
        return responses.get(3);
    }

    public List<String> getResponses() {
        return new ArrayList<>(responses);
    }

    public List<String> getRandomizedResponses() {
        List<String> randomizedResponses = new ArrayList<>(responses);
        Collections.shuffle(randomizedResponses);
        return randomizedResponses;
    }

    public void setResponses(List<String> responses) {
        this.responses = responses;
    }

    public static final int NB_RESPONSES = 4;

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        for (int i = 0; i < NB_RESPONSES; i++) {
            dest.writeString(responses.get(i));
        }
    }

    public static final Parcelable.Creator<MultipleChoiceStep> CREATOR = new Parcelable.Creator<MultipleChoiceStep>() {

        @Override
        public MultipleChoiceStep createFromParcel(Parcel in) {
            return new MultipleChoiceStep(in);
        }

        @Override
        public MultipleChoiceStep[] newArray(int size) {
            return new MultipleChoiceStep[size];
        }
    };
}
