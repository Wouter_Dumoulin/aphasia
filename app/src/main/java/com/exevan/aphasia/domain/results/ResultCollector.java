package com.exevan.aphasia.domain.results;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.esotericsoftware.yamlbeans.YamlWriter;
import com.exevan.aphasia.config.Defaults;
import com.exevan.aphasia.domain.TestHandleFactory;
import com.exevan.aphasia.domain.tests.TestHandle;
import com.exevan.aphasia.io.IOUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Exevan on 29/12/2016.
 */

public class ResultCollector implements Parcelable {

    private Context context;

    private TestHandle test;
    private String tempDir;
    private String resultDir;
    private String resultFile;

    private Results results;

    public ResultCollector(Context context, TestHandle test, String resultDir) {
        this.context = context;

        this.test = test;

        this.tempDir = Defaults.TEMP_DIR + "/temp_" + test.getName();
        resetTempDir();

        this.resultDir = resultDir;

        this.results = new Results(test.getName());
    }

    public ResultCollector(Parcel in) {
        test = new TestHandleFactory().createTestHandleFromFile(new File(in.readString()));
        tempDir = in.readString();
        resultDir = in.readString();
        results = new Results();

        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            int step = in.readInt();
            String hint = in.readString();
            String response = in.readString();
            results.addEntry(step, hint, response);
        }
    }

    public String getResultFile() {
        return this.resultFile;
    }

    public void resetTempDir() {
        clearTempDir();

        File tempDirFile = new File(tempDir);
        tempDirFile.mkdirs();
        IOUtil.scanFile(context, tempDirFile);
    }

    public void clearTempDir() {
        File tempDirFile = new File(tempDir);
        if(tempDirFile.exists())
            IOUtil.deleteRecursive(tempDirFile);
    }

    public File getTempResultDir() {
        return new File(tempDir);
    }

    public String getHint(int i) {
        return results.getHint(i);
    }

    public String getResponse(int i) {
        return results.getResponse(i);
    }

    public void addResult(int step, String hint, String response) {
        results.addEntry(step, hint, response);
    }

    public void saveResults() {

        try {

            YamlWriter writer = new YamlWriter(new FileWriter(getTempResultDir().getAbsolutePath() +"/mapping.yaml"));
            writer.getConfig().setClassTag("results", Results.class);
            writer.getConfig().setPropertyElementType(Results.class, "entries", ResultEntry.class);
            writer.write(results);
            writer.close();

            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String timeStamp = format.format(new Date());

            File resultFile = new File(resultDir + "/result_" + test.getName() + "_" + timeStamp + ".zip");
            IOUtil.zip(getTempResultDir().listFiles(), resultFile);

            IOUtil.scanFile(context, resultFile);

            clearTempDir();

            this.resultFile = resultFile.getAbsolutePath();

        } catch (IOException e) {
            Log.e("Aphasia", Log.getStackTraceString(e));
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(test.getPath());
        dest.writeString(tempDir);
        dest.writeString(resultDir);

        dest.writeInt(results.getEntries().size());
        for(ResultEntry result : results.getEntries()) {
            dest.writeInt(result.step);
            dest.writeString(result.getHint());
            dest.writeString(result.getResponse());
        }
    }

    public static final Parcelable.Creator<ResultCollector> CREATOR = new Parcelable.Creator<ResultCollector>() {

        @Override
        public ResultCollector createFromParcel(Parcel in) {
            return new ResultCollector(in);
        }

        @Override
        public ResultCollector[] newArray(int size) {
            return new ResultCollector[size];
        }
    };
}
