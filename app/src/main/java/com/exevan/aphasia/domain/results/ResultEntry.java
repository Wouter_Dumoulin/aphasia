package com.exevan.aphasia.domain.results;

import java.io.Serializable;

/**
 * Created by Exevan on 5/01/2017.
 */
public class ResultEntry implements Serializable{

    public int step;
    public String hint;
    public String response;

    public ResultEntry() {
    }

    public ResultEntry(int step, String hint, String response) {
        this.step = step;
        this.hint = hint;
        this.response = response;
    }

    public String getHint() {
        return hint;
    }

    public String getResponse() {
        return response;
    }
}
