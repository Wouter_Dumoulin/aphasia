package com.exevan.aphasia.io;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.android.vending.expansion.zipfile.ZipResourceFile;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Exevan on 19/12/2016.
 */

public class IOUtil {

    public static String toJSON(Object o) {
        Gson gson = new Gson();
        return gson.toJson(o);
    }

    public static <T> T fromJSON(String json, Class<T> c) {
        Gson gson = new Gson();
        return gson.fromJson(json, c);
    }

    public static boolean deleteRecursive(File f) {
        boolean success = true;
        if (f.isDirectory())
            for (File child : f.listFiles())
                success = success && deleteRecursive(child);

        return success && f.delete();
    }

    public static String getFileNameFromPath(String path) {
        String tmp = path.split("\\.")[0];
        String[] tmp2 = tmp.split("/");
        return tmp2[tmp2.length - 1];
    }

    public static Bitmap getImageFromZip(String imgPath, String zipPath) {
        try {
            ZipResourceFile zrf = new ZipResourceFile(zipPath);
            Log.d("Aphasia", imgPath);
            Bitmap img = BitmapFactory.decodeStream(zrf.getAssetFileDescriptor(imgPath).createInputStream());
            if (img.getWidth() != img.getHeight()) {
                Log.e("Aphasia", "could not load " + imgPath + ", image is not square");
                throw new RuntimeException();
            }
            return img;
        } catch (IOException e) {
            Log.e("Aphasia", "could not load image " + imgPath);
            return null;
        }
    }

    public static void zip(File[] files, File zip) {
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zip);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[BUFFER];

            for (File file : files) {
                Log.v("Compress", "Adding: " + file);
                FileInputStream fi = new FileInputStream(file);
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void scanFile(Context context, File file) {
        Intent intent =
                new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(file));
        context.sendBroadcast(intent);
    }

    private static final int BUFFER = 4096;

}
